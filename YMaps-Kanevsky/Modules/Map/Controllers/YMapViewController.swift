//
//  ViewController.swift
//  YMaps-Kanevsky
//
//  Created by Eduard Kanevskii on 23.05.2024.
//

import UIKit

final class YMapViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    private func setupUI() {
        view.backgroundColor = .systemBlue
    }

}

